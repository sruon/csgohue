import os

class Config:
    HUB_IP = os.environ.get("HUB_IP", None)
    GROUP_NAME = os.environ.get('GROUP_NAME', None)

if not Config.HUB_IP or not Config.GROUP_NAME:
    raise Exception('HUB_IP and GROUP_NAME environment variables must be defined')