# Usage

`docker run --env HUB_IP="10.10.10.10" --env GROUP_NAME="Gaming PC" -p 3000:3000 sruon/csgohue`

Replace the `HUB_IP` with your Phillips Hue Hub IP and `GROUP_NAME` with the name of a lights group defined on your Hub.

# Configure CSGO

Drop the `gamestate_integration_csgohue.cfg` in your CSGO folder in the `cfg` folder. Replace the IP with your docker endpoint (i.e. "http://127.0.0.1:3000")

### Error: phue.PhueRegistrationException: (101, 'The link button has not been pressed in the last 30 seconds.')

Push the link button on your Hue hub before starting
