import falcon
from phue import Bridge
import time
import threading
from config import Config


class HueManager:
    def __init__(self):
        self.bridge = Bridge(Config.HUB_IP)
        self.bridge.connect()
        self.bridge.get_api()
        self.group_id = self.bridge.get_group_id_by_name(Config.GROUP_NAME)
        self.group = self.bridge.get_group(Config.GROUP_NAME)

    def set_lights(self):
        self.bridge.set_group(self.group_id, "on", True)

    def set_lights_brightness(self, intensity):
        self.bridge.set_group(self.group_id, "bri", intensity)

    def set_lights_color(self, color):
        self.bridge.set_group(self.group_id, color)

    def blink_lights(self, blink_on):
        if blink_on:
            self.set_lights_color(self.red_lights)
            return False
        else:
            self.set_lights_color(self.no_lights)
            return True

    def set_lights_color_single(self, index, color):
        lights = self.bridge.get_group(self.group_id, "lights")
        self.bridge.set_light(int(lights[index]), color)

    def strobe_lights(self, current_strobe_light, previous_strobe_light):
        lights = self.group["lights"]
        if previous_strobe_light is not None and current_strobe_light != previous_strobe_light:
            self.set_lights_color_single(previous_strobe_light, self.no_lights)

        previous_strobe_light = current_strobe_light
        self.set_lights_color_single(current_strobe_light, self.red_lights)
        if current_strobe_light < len(lights) - 1:
            current_strobe_light += 1
        else:
            current_strobe_light = 0
        return current_strobe_light, previous_strobe_light

    @property
    def red_lights(self):
        return {"on": True, "sat": 254, "bri": 254, "hue": 0, "transitiontime": 0}

    @property
    def blue_lights(self):
        return {"on": True, "sat": 254, "bri": 254, "hue": 45000, "transitiontime": 0}

    @property
    def explode_lights(self):
        return {"on": True, "sat": 254, "bri": 254, "hue": 10000, "transitiontime": 0}

    @property
    def white_lights(self):
        return {"on": True, "sat": 0, "bri": 254, "hue": 10000}

    @property
    def no_lights(self):
        return {"on": False, "transitiontime": 0}

    @property
    def freezetime_lights(self):
        return {"on": True, "sat": 125, "bri": 20, "hue": 10000}

    @property
    def live_lights(self):
        return {"on": True, "sat": 125, "bri": 125, "hue": 10000}


class CSGOGameStateResource:
    def __init__(self, hue):
        self.hue = hue
        self.bomb_status = None
        self.bomb_thread = None
        self.is_flashed = False

    def on_flashed(self, intensity):
        if not self.bomb_status:
            if intensity > 20:
                self.is_flashed = True
                print(intensity)
                color = self.hue.white_lights
                color["bri"] = intensity
                self.hue.set_lights_color(color)
                # self.hue.set_lights_brightness(intensity)
            else:
                self.is_flashed = False
                self.on_live()

    def on_ct_win(self):
        self.hue.set_lights_color(self.hue.blue_lights)

    def on_t_win(self):
        self.hue.set_lights_color(self.hue.red_lights)

    def on_freezetime(self):
        self.hue.set_lights_color(self.hue.freezetime_lights)

    def on_live(self):
        if not self.is_flashed:
            self.hue.set_lights_color(self.hue.live_lights)

    def on_explode(self):
        self.bomb_thread = None
        if self.bomb_status != "exploded":
            self.bomb_status = "exploded"
            self.bomb_timer = None
            self.hue.set_lights_color(self.hue.explode_lights)

    def on_defuse(self):
        self.bomb_thread = None
        if self.bomb_status != "defused":
            self.bomb_status = "defused"
            self.bomb_timer = None
            self.hue.set_lights_color(self.hue.blue_lights)

    def on_plant(self):
        last_blink_time = None
        last_strobe_time = None
        current_strobe_light = 0
        previous_strobe_light = None
        blink_on = True

        self.hue.set_lights_color(self.hue.no_lights)
        self.bomb_status = "planted"
        self.bomb_timer = time.time()

        while True:
            if not self.bomb_status:
                return
            if not self.bomb_timer:
                return
            total_bomb_time = time.time() - self.bomb_timer
            if total_bomb_time >= 30:
                if last_blink_time is None:
                    last_blink_time = time.time() - 2
                elapsed_blink_time = time.time() - last_blink_time
                if elapsed_blink_time >= 1:
                    last_blink_time = time.time()
                    blink_on = self.hue.blink_lights(blink_on)
            else:
                if last_strobe_time is None:
                    last_strobe_time = time.time() - 2
                elapsed_strobe_time = time.time() - last_strobe_time
                if elapsed_strobe_time >= 1:
                    last_strobe_time = time.time()
                    current_strobe_light, previous_strobe_light = self.hue.strobe_lights(current_strobe_light, previous_strobe_light)
            print(f"Bomb has been planted for {time.time() - self.bomb_timer}")
            time.sleep(0.20)

    def on_no_bomb(self):
        self.bomb_thread = None
        self.bomb_status = None
        self.bomb_timer = None

    def on_post(self, req, resp):
        player = req.media.get("player")

        round_phase = req.media.get("round", {}).get("phase", None)
        bomb_status = req.media.get("round", {}).get("bomb", None)
        if bomb_status:
            if bomb_status == "exploded":
                self.on_explode()
            if bomb_status == "planted":
                if self.bomb_status != "planted":
                    self.bomb_thread = threading.Thread(target=self.on_plant)
                    self.bomb_thread.start()
            if bomb_status == "defused":
                self.on_defuse()
        else:
            self.on_no_bomb()
        if round_phase == "live" and not bomb_status:
            self.on_live()
        if round_phase == "over":
            winning_team = req.media.get("round", {}).get("win_team", None)
            if winning_team == "CT":
                self.on_ct_win()
            elif winning_team == "T":
                self.on_t_win()
        if round_phase == "freezetime":
            self.on_freezetime()
        is_flashed = player.get("state", {}).get("flashed")
        if is_flashed:
            self.on_flashed(is_flashed)
        resp.status = falcon.HTTP_200


hue = HueManager()
app = falcon.API()
res = CSGOGameStateResource(hue)
app.add_route("/", res)

res.on_live()
