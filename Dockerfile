FROM python:3.6

RUN apt-get update && apt-get upgrade -y

RUN pip install pipenv

WORKDIR /usr/src/app

COPY Pipfile ./
COPY Pipfile.lock ./

RUN pipenv install --system

COPY . .

EXPOSE 3000

ENTRYPOINT gunicorn main:app -b :3000
